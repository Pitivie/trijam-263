extends Node2D

const BACKGROUND = preload("res://assets/sprites/background.png")

var sausage = Rect2(10, 10, 100, 50)

func _draw():
	draw_texture_rect(BACKGROUND, sausage, true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			print("Left button was clicked at ", event.position)
			if sausage.has_point(event.position):
				print("cut")
				sausage = sausage.intersection(Rect2(0, 0, event.position.x, 500))
				queue_redraw()
		if event.button_index == MOUSE_BUTTON_WHEEL_UP and event.pressed:
			print("Wheel up")


func _on_farmer_body_entered(body):
	print(body)
	pass # Replace with function body.


func _on_farmer_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	print(body)
	
	pass # Replace with function body.
